import xml.etree.ElementTree as ET
import os
import copy


def start(file_path, divider_list):
    if not os.path.isfile(file_path):
        raise ValueError("Invalid file path: {}".format(file_path))

    root = ET.parse(file_path).getroot()

    transformEnumerations(None, root, divider_list)

    print("{0}{0}{0}\n\n".format("======"))

    print("RESULT: \n\n")
    ET.dump(root)


def transformEnumerations(parent, current_element, divider_list):
    children = list(current_element)

    if children:
        for child in children:
            transformEnumerations(current_element, child, divider_list)

    for divider in divider_list:
        if divider in current_element.text:

            for divider_to_replace in divider_list:
                current_element.text = current_element.text.replace(divider_to_replace, ',')

            words = current_element.text.split(',')

            if len(words) < 2:
                continue

            id_count = 0
            for word in words:

                new_element = ET.Element(current_element.tag, dict(current_element.attrib))

                if 'ID' in current_element.attrib.keys():
                    new_element.attrib['ID'] += '_' + str(id_count)
                    id_count += 1
                new_element.text = word

                for new_element_child in list(current_element):
                    new_element.append(copy.deepcopy(new_element_child))

                parent.append(new_element)

            print("{0}{0}{0}\n\n".format("======"))

            print("Split element {} into {} sibling elements:\n".format(ET.tostring(current_element), id_count))

            parent.remove(current_element)

            ET.dump(parent)



def duplicate_xml_entry(xml_entry):
    pass


start('test_file.xml', [',', 'and', 'or'])
