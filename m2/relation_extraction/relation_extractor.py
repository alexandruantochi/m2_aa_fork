import os
import re

''' 
current recognized named entities: ORGANIZATION,PERSON,LOCATION,TIME,MONEY,PERCENT,FACILITY,GPE

named entity creation expected from M1: disease, anatomy etc
'''


def demo():
    if not nltk.corpus.ieer:
        nltk.download('ieer')

    IN = re.compile(r'.*\bin\b(?!\b.+ing)')

    print("ORGANIZATION in LOCATION example:")
    for doc in nltk.corpus.ieer.parsed_docs('NYT_19980407'):
        for rel in nltk.sem.extract_rels('ORGANIZATION', 'LOCATION', doc, corpus='ieer', pattern=IN):
            print(nltk.sem.rtuple(rel))

    print("PERSON in LOCATION example:")
    for doc in nltk.corpus.ieer.parsed_docs('NYT_19980407'):
        for rel in nltk.sem.extract_rels('PERSON', 'LOCATION', doc, corpus='ieer', pattern=IN):
            print(nltk.sem.rtuple(rel))


def get_relations(text, relationship_list):
    # demo, create relationship with null param
    if text == 'demo':
        demo()
        return
    if os.path.exists(text):
        if os.path.isfile(text):
            print('Parsing ' + text)
            return
        else:
            return
    elif isinstance(text, dict):
        return
    elif isinstance(text, list):
        return
    elif isinstance(text, str):
        return


def main():
    print('This module should not be run as a standalone module. Please call it from another Python module.')


try:
    import nltk
except:
    print('Please install nltk - http://www.nltk.org/install.html')
    exit()

get_relations('demo', None)

if __name__ == "__main__":
    main()
